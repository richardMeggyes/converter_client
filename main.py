import config, os, time, sys, requests, shutil, preferences
from VideoFile import VideoFile
import converter
import request_handler
from requests_toolbelt.multipart import encoder

username = "readdeo"
password = "asasas"

def download_file(url, fileName):
    # NOTE the stream=True parameter below
    with request_handler.send_request(url, method='GET', stream=True) as r:
        r.raise_for_status()
        file_download_path = os.path.join(config.files_directory, fileName)

        with open(file_download_path, 'wb') as f:
            for chunk in r.iter_content(chunk_size=8192):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    # f.flush()
    if not os.path.isfile(file_download_path):
        raise FileNotFoundError("Download failed, can't find the downloaded file")
    return file_download_path


def send_error(video_file_id):
    url = preferences.server_address + "converter-progress"
    params = {"status": "ERROR", "info": "error while converting", "videoId": video_file_id}
    try:
        request_handler.send_request(url, params, 'POST')
        # requests.post(url, headers=request_handler.get_auth_header(), data=params)
    except Exception as e:
        print("Network error while progress reporting:", e)


def upload_file(video_file):
    print("Uploading file:", video_file.output_path())
    session = requests.Session()

    upload_finished = False
    retry_counter = 0
    while not upload_finished:
        with open(video_file.output_path(), 'rb') as f:
            try:
                form = encoder.MultipartEncoder({
                    "file": (video_file.output_path(), f, "application/octet-stream"),
                    "composite": "NONE",
                    "fileId": str(video_file.id),
                    "client": client_id
                })

                headers = {}
                headers[request_handler.TOKEN_HEADER_KEY] = 'Bearer ' + request_handler.access_token
                headers["Prefer"] = "respond-async"
                headers["Content-Type"] = form.content_type

                response = session.post(preferences.server_address + "converter-job", headers=headers, data=form)
                if response.status_code != 200:
                    print("Upload FAILED! Status code:", response.status_code)
                session.close()
                upload_finished = True

                print("Upload successful")
                return True
            except Exception as e:
                print("Upload failed, retrying...", retry_counter, e)
                retry_counter += 1
                try:
                    session.close()
                except Exception as e:
                    pass
            time.sleep(5)
        if upload_finished:
            os.remove(video_file.output_path())
            os.remove(video_file.path())


if __name__ == "__main__":
    client_id = config.get_client_id()

    prefer_small_files = True

    args = sys.argv

    if len(args) > 1:
        try:
            id = str(args[1])
            client_id += id
            print("My client id is:", client_id)
        except:
            print("Wrong number of repeats was given")
            exit()

    converter.send_info(0, 0, 0, '', '', client_id)
    loop_counter = 0
    while True:

        config.create_dirs()

        url = "converter-job?client=" + client_id

        reservation = request_handler.send_request(url, method='PUT')
        try:
            fileInfo = reservation.json()
            print("reservation:", fileInfo)
        except Exception as e:
            print('File id missing, waiting')
            time.sleep(30)
            continue
        if fileInfo['status'] == 'inactive' or reservation.status_code != 200:
            print('no file to convert')
            time.sleep(30)
            continue

        video_file = VideoFile(fileInfo['fileId'], fileInfo['fileName'], config.files_directory)
        url += "&fileId=" + str(video_file.id)

        print("Downloading file: " + video_file.file_name)
        download_file(url, video_file.file_name)
        print("Converting...")
        convert_success = converter.convert(video_file, client_id)

        if not convert_success:
            send_error(video_file.id)

        upload_finished = False
        while not upload_finished:
            upload_finished = upload_file(video_file)

        # Removing all files in the files folder
        os.remove(video_file.path())
        os.remove(video_file.output_path())

        loop_counter += 1
        print("waiting 10 secs before asking for new file")
        time.sleep(10)

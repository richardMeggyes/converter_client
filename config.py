import os, uuid

files_directory = "files"

def get_client_id():
    worker_id = 'worker.id'
    if not os.path.isfile(worker_id) or os.stat(worker_id).st_size < 8:
        new_uuid = str(uuid.uuid1())[:8]
        with open(worker_id, "w+") as f:
            f.write(new_uuid)
        return new_uuid
    else:
        client_uuid = ""
        with open(worker_id, 'r') as f:
            client_uuid = f.readline()
        return client_uuid


def create_dirs():
    if not os.path.isdir(files_directory):
        os.mkdir(files_directory)


def read_name():
    with open('name.txt', "r") as f:
        return f.readline()
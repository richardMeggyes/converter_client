import os, subprocess, re, time
from preferences import max_width, FFmpeg, FFmpeg_preset, FFmpeg_acodec, FFmpeg_vcodec
from terminal_handler import printProgressBar
from error_handler import write_error_log
# from config import base_url
import request_handler


def get_video_file_width(file):

    if os.name != 'nt':
        cmd_mediainfo = "mediainfo '--Inform=Video; %Width%'" + " \"" + file + "\""
        print("mediainfo_width_cmd", cmd_mediainfo)
        video_width = os.popen(cmd_mediainfo).read()
        if ("command not found" in video_width):
            return "error"
        video_width = video_width.split("/")[0]
        print("video-width:", video_width)
        return video_width

    else:
        # Windows version with ffmpeg
        ffprobe_cmd = "ffprobe -v error -select_streams v:0 -show_entries stream=width -of csv=s=x:p=0 " + " \"" + file + "\""
        width = os.popen(ffprobe_cmd).read().replace("\n", "")
        print("width:" + width + ":")
        return width


def get_frames_count(input_file):
    print("Counting number of frames in file...")
    if os.name != 'nt':
        asd = os.popen('mediainfo --fullscan "{}"'.format(input_file)).readlines()
        for line in asd:
            # print("MEDINFO ", line)
            if "Frame count" in line:
                line = line.split(": ")
                return int(line[1].replace(" ", ""))
        return 1
    else:
        ffprobe_count_frames_cmd = "ffprobe -v error -count_frames -select_streams v:0 -show_entries stream=nb_read_frames -of default=nokey=1:noprint_wrappers=1 "  + " \"" + input_file + "\""
        frames = os.popen(ffprobe_count_frames_cmd).readlines()[0]
        return frames


def resize_video(path):
    # Whitespace needs to be in the beginning if the rescale string
    FFmpeg_rescale = ' -vf "scale={}:-1, fps=fps=24" '.format(max_width)
    # Getting video's width using mediainfo
    get_width = get_video_file_width(path)
    if get_width == "error":
        return FFmpeg_rescale
    video_file_width = int(get_width)  # Getting width of video file
    print('Width of video file / max width: {}/{}'.format(video_file_width, max_width))

    try:
        if int(video_file_width) > max_width:  # Setting output file's width
            print("Resizing video to: {}".format(max_width))
            return FFmpeg_rescale
        else:
            print("No resizing needed")
            FFmpeg_rescale = ' -vf "fps=fps=24" '
    except Exception as e:
        print("Unexpected error happened while getting video width:", e)
        print("Current value: ", get_width)
    return FFmpeg_rescale


def convert(video_file, client_id):
    start_time = time.time()
    progress_info = time.time()
    failed = False
    i = 0
    frames_number = get_frames_count(video_file.path())

    cmd = converter_command(video_file.path(), video_file.output_path())

    print("converter command:", cmd)
    with open("commands.txt", 'a+') as f:
        f.write(cmd + "\n")

    with open("ffmpeg.txt", 'a+') as f:
        f.write(cmd + "\n")

    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, universal_newlines=True, shell=True)

    for line in iter(p.stdout.readline, b''):
        line = str(line.rstrip())  # [2:-1]
        with open("ffmpeg.txt", 'a+') as f:
            if not "frame=" in line:
                f.write(line + "\n")
        if "Invalid data found when processing input" in line:
            write_error_log("Error in file: " + video_file.file_name + " ffmpeg message:" + line)
            failed = True
            break
        if "Conversion failed!" in line:
            write_error_log("Error in file: " + video_file.file_name + " ffmpeg message:" + line)
            failed = True
            break

        if "frame=" in line:
            try:
                line = re.sub(' +', ' ', line).replace("= ", "=").split(" ")
                frame = int(line[0].split("=")[1])
                elapsed_time = time.strftime("%H:%M:%S", time.gmtime(time.time() - start_time))
                print_progress(get_fps(line), elapsed_time, frame, frames_number)
                i += 1
                if progress_info < time.time():
                    send_info(get_fps(line), frame, frames_number, elapsed_time,
                              get_remaining_time_string(frames_number, frame, get_fps(line)), client_id)
                    progress_info = time.time() + 5.0
            except Exception as e:
                print(e)
                print(line)
        else:
            if len(line) == 0 and p.poll() is not None:
                break
            print(">>> " + line)
    if failed:
        return False
    return True


def print_progress(fps, elapsed_time, frame, frames_number):
    progressbar = printProgressBar(frame, int(frames_number), length=50)
    r_day, r_hour, r_minute, r_second = get_split_time(get_estimated_time_sec(frames_number, frame, fps))

    processed_line = ("%58s FPS: %5s E: %8s R: %2i:%2s:%2s" % (progressbar, fps, elapsed_time,
                                                               r_hour, r_minute, r_second))
    print(processed_line, end="\r")


def get_remaining_time_string(frames_number, frame, fps):
    r_day, r_hour, r_minute, r_second = get_split_time(get_estimated_time_sec(frames_number, frame, fps))
    return "{}:{}:{}".format(r_hour, r_minute, r_second)


def converter_command(input_path, output_path):
    return FFmpeg + \
           " \"" + \
           input_path + \
           "\"" + \
           resize_video(input_path) + \
           FFmpeg_preset + \
           " " + \
           FFmpeg_acodec + \
           " " + \
           FFmpeg_vcodec + \
           " \"" + \
           output_path + \
           "\""


def get_split_time(estimated_time_sec):
    r_day = int(time.strftime("%d", time.gmtime(estimated_time_sec))) - 1
    r_hour = int(time.strftime("%H", time.gmtime(estimated_time_sec))) + (r_day * 24)
    r_minute = time.strftime("%M", time.gmtime(estimated_time_sec))
    r_second = time.strftime("%S", time.gmtime(estimated_time_sec))

    return r_day, r_hour, r_minute, r_second


def get_estimated_time_sec(frames_number, frame, fps):
    try:
        return (float(frames_number) - float(frame)) / fps
    except Exception as e:
        print(e)
        return 1


def get_fps(line):
    fps = line[1].split("=")[1]
    if fps == "0.0": fps = "1.1"
    return float(fps)


def send_info(fps, frame, total_frames, elapsed_time, remaining_time, client_id):
    url = "converter-progress"
    data = {"status": "converting", "fps": float(fps), "frame": frame, "totalFrames": total_frames,
              "client": client_id, "elapsedTime": elapsed_time, "remainingTime": remaining_time}
    try:
        #requests.post(url, headers=request_handler.get_auth_header(), params=data)
        request_handler.send_request(url, data, method="POST")

    except Exception as e:
        print("Network error while progress reporting:", e)

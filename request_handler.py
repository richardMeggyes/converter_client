import requests, json, main, preferences

TOKEN_HEADER_KEY = 'Authorization'

access_token = ""


def get_default_headers():
    return {'Content-Type': 'application/json'}


def get_auth_header():
    if access_token == "":
        get_auth_access_token(main.username, main.password)
    header = {'Content-Type': 'application/json', TOKEN_HEADER_KEY: 'Bearer ' + access_token}
    return header


def get_auth_access_token(username, password):
    data = {'username': username, 'password': password}
    response = requests.post(preferences.server_address + preferences.login_url, headers=get_default_headers(), data=json.dumps(data))
    response_json = json.loads(response.text)
    global access_token
    access_token = response_json['accessToken']
    return response_json['accessToken']


def send_request(url, data={}, method='GET', stream=False):
    counter = 0
    while counter < 10:
        headers = get_auth_header()
        headers[TOKEN_HEADER_KEY] = 'Bearer ' + access_token
        if method == 'GET':
            response = requests.get(preferences.server_address + url, headers=headers, params=data, stream=stream)
            if not if_unauthorized(response):
                return response
        if method == 'POST':
            response = requests.post(preferences.server_address + url, headers=headers, params=data, stream=stream)
            if not if_unauthorized(response):
                return response
        if method == 'PUT':
            response = requests.put(preferences.server_address + url, headers=headers, params=data, stream=stream)
            if not if_unauthorized(response):
                return response
        if method == 'DELETE':
            response =  requests.delete(preferences.server_address + url, headers=headers, params=data, stream=stream)
            if not if_unauthorized(response):
                return response
        print("Unauthorised. Retries:", counter)


def if_unauthorized(response):
    if response.status_code == 401:
        global access_token
        access_token = ""
        print("Unauthorised, re-authenticating")
        get_auth_access_token(main.username, main.password)
        return True
    return False

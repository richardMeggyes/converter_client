import os


class VideoFile:
    def __init__(self, id, file_name, dir_path):
        self.id = id
        self.file_name = file_name
        self.dir_path = dir_path

    def path(self):
        return os.path.join(self.dir_path, self.file_name)

    def output_path(self):
        output_file_name = os.path.join(self.dir_path, "C_" + self.file_name)
        output_file_name_ex = output_file_name.split('.')
        extension_len = len(output_file_name_ex[len(output_file_name_ex) - 1])

        double_sep = True
        while double_sep:
            separator = os.sep
            if separator + separator in output_file_name:
                output_file_name.replace(separator + separator, separator)
            else:
                double_sep = False
        return os.path.join(output_file_name[:-extension_len] + 'mp4')
